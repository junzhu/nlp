#! /usr/bin/python3

import os
import sys
import glob
import json
import math
import logging
from collections import Counter
import matplotlib.pyplot as plt
from datetime import datetime

lemmaDict = {}

# Create a custom formatter to include timestamp along with the message
class TimestampFormatter(logging.Formatter):
    def format(self, record):
        record.timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
        return super().format(record)

# Set up logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)

stream_handler = logging.StreamHandler(sys.stdout)
stream_formatter = logging.Formatter('%(message)s')
stream_handler.setFormatter(stream_formatter)
logger.addHandler(stream_handler)

file_handler = logging.FileHandler('output.log', mode='w')  # Change 'logfile.log' to desired filename
formatter = logging.Formatter('%(message)s')
file_handler.setFormatter(formatter)

logger.addHandler(file_handler)

# Example usage: Log a specific message with a timestamp
def log_with_timestamp(message):
    # Temporarily change the formatter for this log record to include a timestamp
    file_handler.setFormatter(TimestampFormatter('%(asctime)s %(message)s'))
    logger.info(message)
    # Reset the formatter back to the original one
    file_handler.setFormatter(formatter)

def count_word_frequency_from_corenlp_output(filename):
    word_counts = Counter()
    word_counts_duo = Counter()
    with open(filename, 'r', encoding='utf-8') as file:
        data = json.load(file)
        total_words = 0
        for sentence in data['sentences']:
            total_words += len(sentence['tokens'])
            for token in sentence['tokens']:
                lemmaDict[token['word']] = token['lemma']  # Add into lemmaDict
            for dependency in sentence['basicDependencies']:
                if dependency["dep"] == "amod":
                    word1 = lemmaDict[dependency["governorGloss"]]
                    word_counts[word1] += 1
                    word2 = lemmaDict[dependency["dependentGloss"]]
                    word_counts[word2] += 1
                    word_counts_duo[(word1, word2)] += 1
    return word_counts, word_counts_duo, total_words

def report_word_count(word_counts, word_counts_duo, total_words):
    logging.info("--- word counts frequency")
    totalCount = 0
    for word, count in word_counts.items():
        totalCount += count
        logging.info(f"{word}: {count}")

    logging.info("--- combination counts frequency")
    logging.info(f"D:  {total_words}")
    for word, count in word_counts_duo.items():
        A = count
        B = word_counts[word[0]]
        C = word_counts[word[1]]
        D = total_words
        MI = math.log2( A*D / (B*C))
        logging.info(f"{word}: {A}, {word[0]} : {B}, {word[1]} : {C}, MI : {MI} ")

def plot_dict(data):
    plt.bar(data.keys(), data.values())
    plt.xlabel('Word Lemmas')
    plt.ylabel('Count')
    plt.title('Word Count Frquency')
    plt.xticks(rotation=60)
    plt.savefig('output.png', bbox_inches='tight')
    plt.show()

if __name__ == "__main__":
    json_data_path = "./"
    if len(sys.argv) == 2:
        json_data_path = sys.argv[1] + '/'
        log_with_timestamp("Json file dir is: " + sys.argv[1])

    accu_word_counts, accu_word_counts_duo = Counter(), Counter()
    accu_total_words = 0
    files = glob.glob(json_data_path + '*.json', recursive=False)
    for filename in files:
        # Check json files only
        if not filename.endswith('.json'):
            continue
        filename = os.path.join(os.path.dirname(__file__), filename)
        word_counts, word_counts_duo, total_words = count_word_frequency_from_corenlp_output(filename)
        accu_word_counts += word_counts
        accu_word_counts_duo += word_counts_duo
        accu_total_words += total_words
        log_with_timestamp("=== File name: " + filename)
        report_word_count(word_counts, word_counts_duo, total_words)
        logging.info("")

    # Report accumulated results
    log_with_timestamp("=== Accumulated results: ")
    report_word_count(accu_word_counts, accu_word_counts_duo, accu_total_words)
    # plot_dict(accu_word_counts)
