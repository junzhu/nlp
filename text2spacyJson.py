import spacy
import json
import os
import sys
import glob
import shutil
import argparse
from pathlib import Path

def spacy_to_corenlp_json(doc):
    sentences = []
    for sent in doc.sents:
        tokens = []
        dependencies = []
        for token in sent:
            head = token.head
            token_info = {
                "index": token.i + 1,
                "word": token.text,
                "originalText": token.text,
                "lemma": token.lemma_,
                "characterOffsetBegin": token.idx,
                "characterOffsetEnd": token.idx + len(token.text),
                "pos": token.pos_,
                "ner": token.ent_type_ if token.ent_type_ else "O",
                "before": token.text,
                "after": "",
                "speaker": "UNKNOWN",
            }
            tokens.append(token_info)
            dependencies.append({
                "dependent": token.i + 1,
                "dependentGloss": token.text,
                "governor": head.i + 1,
                "governorGloss": head.text,
                "dep": "ROOT" if token.dep_ == "ROOT" else token.dep_,
                "head.lemma": head.lemma_
            })
        sentences.append({"tokens": tokens, "basicDependencies": dependencies})
    return {"sentences": sentences}

def analysis(txt_data_path, output_spacy_dirpath, output_excel=False):
    files = glob.glob(txt_data_path + '/*.txt', recursive=False)
    for filename in files:
        print("Analyzing with spaCy on - ", filename)
        file = open(filename, "r", encoding="utf-8")
        text = file.read()
        # Process text with spaCy
        doc = nlp(text)
        # Convert spaCy output to CoreNLP-compatible JSON
        corenlp_json = spacy_to_corenlp_json(doc)

        # Write CoreNLP-compatible JSON output to file
        output_file = os.path.join(output_spacy_dirpath, os.path.basename(filename) + ".json")
        with open(output_file, "w", encoding="utf-8") as output_json_file:
            json.dump(corenlp_json, output_json_file, ensure_ascii=False, indent=2)
            print("Output spaCy analysis to -", output_json_file.name)

        # Write JSON data to Excel file
        if output_excel:
            import pandas as pd
            output_file = os.path.join(output_spacy_dirpath, os.path.basename(filename) + ".xlsx")
            df = pd.json_normalize(corenlp_json['sentences'], 'tokens', ['basicDependencies'])
            df.to_excel(output_file, index=False)
            print("Output spaCy analysis to -", output_file)

        print()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process text with spaCy and output results to JSON and Excel files.")
    parser.add_argument("input_dir", help="Path to the input text file")
    parser.add_argument("--with-excel", action="store_true", help="Output Excel file")
    args = parser.parse_args()

    print("args.with_excel: ", args.with_excel)
    print("args.input_dir: ", args.input_dir)
    # main(output_excel=args.with_excel)

    input_dir = args.input_dir
    print("TXT files dir is: " + input_dir)
    output_spacy_dirpath = Path(input_dir) / 'output_spacy_json'
    if output_spacy_dirpath.exists() and output_spacy_dirpath.is_dir():
        shutil.rmtree(output_spacy_dirpath)
    os.mkdir(output_spacy_dirpath)
    print("Created a new output dir - ", output_spacy_dirpath)
    print()

    nlp = spacy.load("en_core_web_lg")
    analysis(input_dir, output_spacy_dirpath, output_excel=args.with_excel)
    print("Done!!!")
