# How to use

Get all the files to be analyzed under ./data directory. Then, run following command to generate spaCy parsed files.
> python text2spacyJson.py ./data/

If Excel files are needed, run with the specified option instead
> python text2spacyJson.py ./data/ --with-excel

Calculate word/combination count frequency, and MI. Check results in output.log file.
> python count.py data/output_spacy_json/
